# Desafío Técnico Europ Assistance

### Descripción
* Aplicación de consola, que cuenta con 2 funciones, la primera simplifica a la mínima expresión las fracciones que pasen por parámetro y la segunda valida la correcta forma de escribir un nombre que pasen por parámetro.

### Tecnologías
* App de Consola (.NET Framework 4.7.2 - C#)   
 
