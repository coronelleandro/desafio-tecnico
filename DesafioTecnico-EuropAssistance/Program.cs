﻿using DesafioTecnico_EuropAssistance.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioTecnico_EuropAssistance
{
    class Program
    {
        static void Main(string[] args)
        {
            //Probando metodo de simplificar una fraccion
            Console.Write("1. Resultado : " + Simplicar("140/260") + "\n");
            Console.ReadKey();

            //Definiendo el simplificar una fraccion
            string Simplicar(string fraccion)
            {
                string primerNum = ""; string segundoNum = ""; int count = 0;

                for (int i = 0; i < fraccion.Length; i++)
                {
                    if (count == 0)
                    {
                        if (true == esUnNumero(fraccion.Substring(i, 1)))
                        {
                            primerNum += fraccion.Substring(i, 1);
                        }
                        if (fraccion.Substring(i, 1) == "/")
                        {
                            count++;
                        }
                    }
                    else
                    {
                        segundoNum += fraccion.Substring(i, 1);
                    }
                }

                int num1 = int.Parse(primerNum);
                int num2 = int.Parse(segundoNum);
                int numMenor = asignarMenor(num1, num2);
                int numMayor = asignarMayor(num1, num2);
                int maximoComunDivisor = buscarMaximoComunDivisor(numMenor, numMayor);
                num1 = num1 / maximoComunDivisor;
                num2 = num2 / maximoComunDivisor;

                return num1 + "/" + num2;
            }

            bool esUnNumero(string str)
            {
                try
                {
                    int.Parse(str);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            int asignarMayor(int num1, int num2)
            {
                if (num1 < num2) return num2;
                else return num1;
            }

            int asignarMenor(int num1, int num2)
            {
                if (num1 < num2) return num1;
                else return num2;
            }

            int buscarMaximoComunDivisor(int numMenor, int numMayor)
            {
                int numDisible = 0; bool validar = false;
                List<int> listDivisible = new List<int>(); int maximoComunDivisor = 1;
                for (int i = 2; i <= numMenor; i++)
                {
                    if ((numMenor % i) == 0)
                    {
                        numDisible = i;
                        validar = true;
                    }

                    if (numMayor % i == 0 && numDisible != 0)
                    {
                        if (numMayor % numDisible == 0 && validar == true)
                        {
                            maximoComunDivisor = numDisible;
                            validar = false;
                        }
                    }
                }
                return maximoComunDivisor;
            }


            //=====================================================================
            //Probando el metodo de validar Nombre
            Console.Write("2. Resultado : " + ValidName("Edgard Allan Poe"));
            Console.ReadKey();

            //Definiendo el metodo de validar nombre
            bool ValidName(string nombreCompleto)
            {
                string nombre = nombreCompleto.Trim();
                List<string> nombres = new List<string>();
                string nombreApellido = "";
                ValidNameModel validNameModel = new ValidNameModel();
                for (int i = 0; i < nombre.Length; i++)
                {
                    if (nombre.Substring(i, 1) == " ")
                    {
                        nombres.Add(nombreApellido);
                        nombreApellido = "";
                    }
                    else if (i == (nombre.Length - 1))
                    {
                        nombreApellido += nombre[i];
                        nombres.Add(nombreApellido);
                    }
                    else
                    {
                        nombreApellido += nombre[i];
                    }
                }
                validNameModel = validarCantidadDeNombres(validNameModel, nombres);
                return validNameModel.resultado;
            }

            ValidNameModel validarCantidadDeNombres(ValidNameModel validNameModel, List<string> nombres)
            {
                if (nombres.Count == 1) validNameModel.resultado = false;
                else if (nombres.Count == 2) validNameModel = unNombreYApellido(validNameModel, nombres);
                else if (nombres.Count == 3) validNameModel = dosNombresYApellido(validNameModel, nombres);
                else validNameModel.resultado = false;
                return validNameModel;
            }

            ValidNameModel unNombreYApellido(ValidNameModel validNameModel, List<string> nombres)
            {
                foreach (var nom in nombres.Select((value, index) => new { value, index }))
                {
                    if (nom.value.Length < 2) validNameModel.resultado = false;
                    if (nom.index == 0) validNameModel = validarNombrePorLetra(validNameModel, nom.value, nom.index);
                    else validNameModel = validarApellidoPorLertra(validNameModel, nom.value);
                }
                return validNameModel;
            }

            ValidNameModel dosNombresYApellido(ValidNameModel validNameModel, List<string> nombres)
            {
                foreach (var nom in nombres.Select((value, index) => new { value, index }))
                {
                    if (nom.value.Length < 2) validNameModel.resultado = false;

                    if (nom.index == 0) validNameModel = validarNombrePorLetra(validNameModel, nom.value, nom.index);
                    else if (nom.index == 1) validNameModel = validarNombrePorLetra(validNameModel, nom.value, nom.index);
                    else validNameModel = validarApellidoPorLertra(validNameModel, nom.value);
                }

                if (validNameModel.primerNombreAbreviado == true)
                {
                    if (validNameModel.segundoNombreAbreviado == false)
                    {
                        validNameModel.resultado = false;
                    }
                }
                return validNameModel;
            }

            ValidNameModel validarNombrePorLetra(ValidNameModel validNameModel, string nombre, int numIndex)
            {
                foreach (var letra in nombre.Select((value, index) => new { value, index }))
                {
                    if ((letra.value >= 'A' && letra.value <= 'Z') && letra.index == 0) { }
                    else if (letra.index == 1 && letra.value.ToString().Substring(0, 1) == ".")
                    {
                        if (letra.index < (nombre.Length - 1)) validNameModel.resultado = false;
                        else
                        {
                            if (numIndex == 0) validNameModel.primerNombreAbreviado = true;
                            else validNameModel.segundoNombreAbreviado = true;
                        }
                    }
                    else if ((letra.value >= 'a' && letra.value <= 'z') && letra.index != 0) { }
                    else validNameModel.resultado = false;
                }
                return validNameModel;
            }

            ValidNameModel validarApellidoPorLertra(ValidNameModel validNameModel, string nombre)
            {
                foreach (var letra in nombre.Select((value, index) => new { value, index }))
                {
                    if ((letra.value >= 'A' && letra.value <= 'Z') && letra.index == 0) { }
                    else if ((letra.value >= 'a' && letra.value <= 'z') && letra.index != 0) { }
                    else validNameModel.resultado = false;
                }
                return validNameModel;
            }
        }
    }
}
