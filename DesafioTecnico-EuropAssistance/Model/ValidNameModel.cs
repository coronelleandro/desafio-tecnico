﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioTecnico_EuropAssistance.Model
{
    class ValidNameModel
    {
        public ValidNameModel()
        {
            resultado = true;
            primerNombreAbreviado = false;
            segundoNombreAbreviado = false;
        }

        public bool resultado { set; get; }
        public bool primerNombreAbreviado { set; get; }
        public bool segundoNombreAbreviado { set; get; }
    }
}
